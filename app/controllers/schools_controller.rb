class SchoolsController < ApplicationController
	before_action :set_school, only: [:update, :destroy, :show]

	#respond_to :json

	#GET /schools
	def index
		@schools = School.all
		# respond_to do |format|
	 	#  format.json { render json: @schools }
	 	# end
	end

	#GET /schools/[:id]
	def show
	end

	#POST /schools
	def create
		@school = School.new(school_params)
		@school.save
	end

	#DELETE /schools/[:id]
	def destroy
		@school.destroy 
	end

	#PUT /schools/[:id]
	def update
		@school.update(school_params)
	end

	private

		def set_school
	      @school = School.find(params[:id])
	    end

		def school_params
			params.require(:school).permit(:name, :address, :director)
		end
end
